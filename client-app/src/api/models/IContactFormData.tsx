export default interface IContactFormData {
  company: string;
  name: string;
  email: string;
  message: string;
}
